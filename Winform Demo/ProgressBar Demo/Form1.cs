﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgressBar_Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
        }

        private int _value = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //this.progressBar1.Increment(1);
            _value++;
            this.progressBar1.Value = _value;
            label1.Text = _value.ToString();
            System.Threading.Thread.Sleep(100);//It's able to use Thread.Sleep with Timer as well
            //Using Timer might be the most simple way to show the progress bar and lable 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Create a new task and trigger the timer soon, no wait task
            var task = Task.Run(() =>
            {
                System.Threading.Thread.Sleep(10000);//The Timer will wait for 10s after the task finish with Wait
            });
            timer1.Start();
            //Task.WaitAll(task); // Without waiting the task, the progress bar working well (try the test with waiting, the progress bar is not working well )
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var f = new _FormProgressBar();
            f.ShowDialog();
        }

        

        private void button4_Click(object sender, EventArgs e)
        {
            _value = 0;
            while (_value <= 1000)
            {
                this.progressBar1.Value = _value;
                label1.Text = _value.ToString();
                //System.Threading.Thread.Sleep(1000);//Sleep will cause the screen being locked
                //Use Timer with Thread.Sleep(1000)
                _value++;

            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label1.Text = e.ProgressPercentage.ToString();
            progressBar1.Value = e.ProgressPercentage;
        }


        private void button6_Click(object sender, EventArgs e)
        {
            var task = Task.Run(() =>
            {
                System.Threading.Thread.Sleep(10000);//The Timer will wait for 10s after the task finish with Wait
            });
            timer1.Start();
            Task.WaitAll(task);
            //Using Task.WaitAll will cause the timer to wait the task as well 
            //After the task finish, the ticker will start (still not know why )
        }
    }
}
